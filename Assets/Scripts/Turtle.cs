﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Turtle
{
    private string _sentence;
    private float _length;
    private float _theta;

    private GameObject _turtle;

    private Stack<Vector3> _positions = new Stack<Vector3>();
    private Stack<Quaternion> _rotations = new Stack<Quaternion>();

    private GameObject _branch;

    public Turtle(string s, float length, float theta, GameObject branch, Vector3 position)
    {
        _sentence = s;
        _length = length;
        _theta = theta;
        _branch = branch;

        _turtle = new GameObject();
        _turtle.name = "Turtle";
        _turtle.transform.position = position;
    }

    public void UpdateSentence(string s)
    {
        _sentence = s;
    }

    public List<Branch> Build()
    {
        List<Branch> branchList = new List<Branch>();

        for(int i = 0; i < _sentence.Length; i++)
        {
            char c = _sentence[i];

            switch(c)
            {
                case 'F':
                    {
                        GameObject branch = MonoBehaviour.Instantiate(_branch, _turtle.transform.position, _turtle.transform.rotation) as GameObject;
                        branchList.Add(branch.GetComponentInChildren<Branch>());
                        _turtle.transform.position += _turtle.transform.up * _length;
                    }
                    break;
                case 'G':
                    {
                        _turtle.transform.position += _turtle.transform.up * _length;
                    }
                    break;
                case '+':
                    {
                        _turtle.transform.rotation *= Quaternion.Euler(0.0f, 0.0f, _theta);
                    }
                    break;
                case '-':
                    {
                        _turtle.transform.rotation *= Quaternion.Euler(0.0f, 0.0f, -_theta);
                    }
                    break;
                case '[':
                    {
                        _positions.Push(_turtle.transform.position);
                        _rotations.Push(_turtle.transform.rotation);
                    }
                    break;
                case ']':
                    {
                        _turtle.transform.position = _positions.Pop();
                        _turtle.transform.rotation = _rotations.Pop();
                    }
                    break;
                case 'S':
                    {
                        _length *= 0.7f;
                    }
                    break;
                   
            }
        }

        return branchList;
    }

    public void Destroy()
    {
        MonoBehaviour.Destroy(_turtle.gameObject);
    }
}
