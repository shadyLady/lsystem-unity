﻿using UnityEngine;
using System.Collections;

public class LSystem 
{
    private string _currentString;
    private Rule[] _ruleset;
    private int _generation;

    public LSystem(string sentence, Rule[] r)
    {
        _currentString = sentence;
        _ruleset = r;
        _generation = 0;
    }

    public void Generate()
    {
        string next = "";

        for(int i = 0; i < _currentString.Length; i++)
        {
            char c = _currentString[i];
            string replace = "" + c;

            for(int j = 0; j < _ruleset.Length; j++)
            {
                char a = _ruleset[j].GetPredecessor();

                if(a == c)
                {
                    replace = _ruleset[j].GetSuccessor();
                    break;
                }
            }

            next += replace;
        }

        _currentString = next;
        _generation++;
    }

    public string GetSentence()
    {
        return _currentString;
    }
    public int GetGeneration()
    {
        return _generation;
    }
}
